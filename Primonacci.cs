﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        // Here we define a new type called D that is a "transformation" type over integers:
        delegate bool D(int i);

        static void Main(string[] args)
        {
        /*    //[][] juicy!
            // Hello! Here we create a list of string:
            List<string> list = new List<string>();

            // Here we add a few items:
            Console.WriteLine("Hit RETURN to add fruits to the list..");
            Console.ReadLine();
            list.Add("Apple");
            list.Add("Orange");
            list.Add("Banana");
            Console.WriteLine("fruits: " + String.Join(", ", list));

            // Here we search for juicy fruits with .NET 1.0 (very old) technology:
            Console.WriteLine("Hit RETURN to find the juicy fruit with .NET 1.0..");
            Console.ReadLine();
            string juicy1 = list.Find(new Predicate<string>(FindInList));
            Console.WriteLine("Juicy fruit is: " + juicy1);

            // Here we search for juicy fruits with .NET 2.0 (old) technology:
            Console.WriteLine("Hit RETURN to find the juicy fruit with .NET 2.0..");
            Console.ReadLine();
            string juicy2 = list.Find(
                delegate (string value)
                {
                    return value == "Orange";
                });
            Console.WriteLine("Juicy fruit is: " + juicy2);
            
            // Here we search for juicy fruits with .NET 3.5 (new) technology, using lambdas:
            Console.WriteLine("Hit RETURN to find the juicy fruit with .NET 3.5..");
            Console.ReadLine();
            string juicy3 = list.Find(value => "Orange" == value);
            Console.WriteLine("Juicy fruit is: " + juicy3);

            //[][] transformations
            // Now that we know how to write lambdas, let's use them:
            // First, let's define a new type called D (D for delegate)
            // Go to the top of the file and look at that definition
            // For example:
            D transformation = (int input) => { int j = 0; return j < input; };

            // What is another name for the transformation above? It takes an integer
            // and returns true if it is strictly bigger than 0, false otherwise
            // For example:
            Console.WriteLine("Hit RETURN to find the transformation of 3 (whether 3 is > than 0)..");
            bool result = transformation(3);
            Console.ReadLine();
            Console.WriteLine(result.ToString());
            */
            // Ok, now that we learned all about lambdas, let's write some corutines with the "yield" keyword
            // Go and find the function called Unfold() a bit down below, right after the FindInList() function,
            // and study it.

            //[][] coroutines
            // Now, let's use that function to build the natural numbers:
            Console.WriteLine("Hit RETURN to generate the natural numbers transofmration..");
            Console.ReadLine();
            var naturalNumbers = Unfold(1, i => i + 1);
            Console.WriteLine("This is how the compiler refers to the transformation:");
            Console.WriteLine(naturalNumbers);
            Console.WriteLine("Now that we have a transformation that describes how natural numbers are built, let's generate a few..");
            Console.ReadLine();

            Console.WriteLine("Hit RETURN to generate the first 10 natural numbers..");
            Console.ReadLine();
            foreach (var x in naturalNumbers.Take(10))
            {
                Console.WriteLine(x);
            }
            Console.ReadLine();

            // Super, let's write a more complicated generator, now, to build Fibonnacci numbers
            // Take a looksie at the function Unfold2() below

            // Now, let's use that function to build the fibonacci numbers:
            Console.WriteLine("Hit RETURN to build the fibonacci transformation..");
            Console.ReadLine();
            var fibonacci = Unfold2(1, (a, b) => a + b);
            Console.WriteLine("This is how the compiler refers to the transformation:");
            Console.WriteLine(fibonacci);
            Console.WriteLine("Now that we have a transformation that describes how fibonacci numbers are built, let's generate a few..");
            Console.ReadLine();
            Console.WriteLine("1st 20 Fibonacci numbers (press return key to generate):");
            Console.ReadLine();
            foreach (var x in fibonacci.Take(20))
            {
                Console.WriteLine(x);
            }
            Console.ReadLine();


            // Wow, how cool is that!
            // Let's do more complicated things..
            // Let's add up the first 20 fibonacci numbers:

            Console.WriteLine("The sum of all 20 first Fibonacci numbers (press return key to generate):");
            Console.ReadLine();
            var result20 = fibonacci.Take(20).Aggregate((a, b) => a + b);
            Console.WriteLine(result20);
            Console.ReadLine();

            Console.WriteLine("The sum of all 20 first **even** Fibonacci numbers (press return key to generate):");
            Console.ReadLine();
            var result20even = fibonacci.Take(20).Aggregate((a, b) => ((a % 2) == 0 ? a : 0) + ((b % 2) == 0 ? b : 0));
            Console.WriteLine(result20even);
            Console.ReadLine();

            Console.WriteLine("The sum of all **even** Fibonacci numbers less than a million (press return key to generate):");
            Console.ReadLine();
            var resultMillion = fibonacci.TakeWhile(p => p <= 1000000).Aggregate((a, b) => ((a % 2) == 0 ? a : 0) + ((b % 2) == 0 ? b : 0));
            Console.WriteLine(resultMillion);
            Console.ReadLine();

            Console.WriteLine("The 101st Fibonacci number (press return key to generate):");
            Console.ReadLine();
            foreach (var x in fibonacci.Skip(100).Take(1))
            {
                Console.WriteLine(x);
            }
            Console.ReadLine();

            Console.WriteLine("Try to find Primonacci numbers ...");
            Console.ReadLine();
            int k = 1;
            foreach (var x in fibonacci) {
                if (k > 100) break;
                if (isPrimeNumber(x))
                {
                    Console.WriteLine("The " + k++ + "th Primonacci is " + x);
                    Console.ReadLine();
                }
                else
                {
                    if(x % 10 == 0) Console.WriteLine(x + " is not valid Primonacci number");
                }
                
            }
            Console.ReadLine();

            // Isn't programming wonderful when you don't have to write ugly code like loops and if-then-else's?
            // You can write code the same way you write english (with a little bit of logic thinking)!
            // You can add breakpoint by hitting F9 on certain lines of your program, and Visual Studio will
            // break on those lines and allow you to inspect variables by hovering over them. Give that a try!

            // We worked with small numbers here, because we made them: int
            // WE could work with bigger numbers, by defining them as: long

            // Now, use this code to write a prime number transformation as a coroutine,
            // Then use it to do your homework!
            //           Console.WriteLine("Use this code to write a prime number transformation as a coroutine,");
            //           Console.WriteLine("Then use it to do your homework!");
            //           Console.WriteLine("G o o d   L u c k!");
            //           Console.ReadLine();

            // bye bye :-)
        }

        private static bool isPrimeNumber(int n)
        {
            if (n < 2) return false;
            if (n == 2 || n == 3) return true;
            if (n % 2 == 0) return false;
            for (int i = 3; i < n / 2; i += 2)
            {
                if (n % i == 0) return false;
            }
            return true;
        }

        // Wow, i'm really far away to where all the action is :-(
        static bool FindInList(string value)
        {
            return value == "Orange";
        }


        // [][] natural numbers transformation (coroutine)
        private static IEnumerable<T> Unfold<T>(T seed, Func<T, T> accumulator)
        {
            var nextValue = seed;
            while (true)
            {
                yield return nextValue;
                nextValue = accumulator(nextValue);
            }
        }

        // [][] fibonacci numbers transofmration (coroutine)
        private static IEnumerable<T> Unfold2<T>(T seed, Func<T, T, T> accumulator)
        {
            var a = seed;
            var b = seed;
            T c;
            while (true)
            {
                yield return b;
                c = b;
                b = accumulator(a, b);
                a = c;
            }
        }
    }
}

