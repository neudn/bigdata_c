﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDataML
{
    class Programb
    {
        private static Random rnd;

        static void Main(string[] args)
        {
            rnd = new Random(0);
            // Example #2
            // Read each line of the file into a string array. Each element
            // of the array is one line of the file.
            Console.WriteLine("Reading files....");
            System.Console.ReadLine();
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\dn\Documents\Visual Studio 2015\Projects\CarDataML\car.data.txt");

            int height = lines.Length;
            String[] entry = lines[0].Split(',');
            int width = entry.Length;

            int[][] matrix = new int[height][];


            // Display the file contents by using a foreach loop.
            System.Console.WriteLine("Contents of WriteLines2.txt = ");
            System.Console.ReadLine();
            for (int i = 0; i < lines.Length; i++)
            {
                // Use a tab to indent each line of the file.
                Console.WriteLine("\t" + lines[i]);
                String[] line = lines[i].Split(',');
                int[] row = new int[22];
                if (line[0].Equals("vhigh")) row[0] = 1;
                else if (line[0].Equals("high")) row[1] = 1;
                else if (line[0].Equals("med")) row[2] = 1;
                else if (line[0].Equals("low")) row[3] = 1;

                if (line[1].Equals("vhigh")) row[4] = 1;
                else if (line[1].Equals("high")) row[5] = 1;
                else if (line[1].Equals("med")) row[6] = 1;
                else if (line[1].Equals("low")) row[7] = 1;

                if (line[2].Equals("2")) row[8] = 1;
                else if (line[2].Equals("3")) row[9] = 1;
                else if (line[2].Equals("4")) row[10] = 1;
                else if (line[2].Equals("5more")) row[11] = 1;

                if (line[3].Equals("2")) row[12] = 1;
                else if (line[3].Equals("4")) row[13] = 1;
                else if (line[3].Equals("more")) row[14] = 1;

                if (line[4].Equals("small")) row[15] = 1;
                else if (line[4].Equals("med")) row[16] = 1;
                else if (line[4].Equals("big")) row[17] = 1;

                if (line[5].Equals("low")) row[18] = 1;
                else if (line[5].Equals("med")) row[19] = 1;
                else if (line[5].Equals("high")) row[20] = 1;

                if (line[6].Equals("anacc")) row[21] = 0;
                if (line[6].Equals("good") || line[6].Equals("vgood") || line[6].Equals("acc")) row[21] = 1;

                matrix[i] = row;
            }

            Console.WriteLine("\nFirst few lines and last of all data are: \n");
            ShowMatrix(matrix, 4, true);

            ShuffleObservations(matrix);

            Console.WriteLine("\nSplitting data into 80% train" + " and 20% test matrices");
            int[][] trainData = null;
            int[][] testData = null;

            // first test case
            Console.WriteLine("\nFirst test case, number is 150: \n");
            int[][] matrixOfInterest1 = new int[150][];
            for (int i = 0; i < 150; i++) {
                matrixOfInterest1[i] = matrix[i];
            }

            Console.WriteLine("\nFirst test case, number is 1000: \n");
            int[][] matrixOfInterest2 = new int[300][];
            for (int i = 0; i < 300; i++)
            {
                matrixOfInterest2[i] = matrix[i];
            }

            MakeTrainTest(matrixOfInterest1, 0, out trainData, out testData);

            Console.WriteLine("\nEncoding 'n' and '?' = 0, 'y' = 1, 'not recommanded' = 0, 'recommanded' = 1");
            Console.WriteLine("Moving final judgement to last column");

            Console.WriteLine("\nBegin training using Winnow algorithm");
            int numInput = 21;
            Winnow w = new Winnow(numInput, 0); // rndSeed = 0
            double[] weights = w.TrainWeights(trainData);
            Console.WriteLine("Training complete");

            Console.WriteLine("\nFinal model weights are:");
            ShowVector(weights, 4, 8, true);

            double trainAcc = w.Accuracy(trainData);
            double testAcc = w.Accuracy(testData);

            Console.WriteLine("\nPrediction accuracy on training data = " + trainAcc.ToString("F4"));
            Console.WriteLine("Prediction accuracy on test data = " + testAcc.ToString("F4"));

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }

        static void MakeTrainTest(int[][] data, int seed, out int[][] trainData, out int[][] testData)
        {
            Random rnd = new Random(seed);
            int totRows = data.Length; // compute number of rows in each result
            int numTrainRows = (int)(totRows * 0.80);
            int numTestRows = totRows - numTrainRows;
            trainData = new int[numTrainRows][];
            testData = new int[numTestRows][];

            int[][] copy = new int[data.Length][]; // make a copy of data
            for (int i = 0; i < copy.Length; ++i)  // by reference to save space
                copy[i] = data[i];
            for (int i = 0; i < copy.Length; ++i) // scramble row order of copy
            {
                int r = rnd.Next(i, copy.Length);
                int[] tmp = copy[r];
                copy[r] = copy[i];
                copy[i] = tmp;
            }
            for (int i = 0; i < numTrainRows; ++i) // create training
                trainData[i] = copy[i];

            for (int i = 0; i < numTestRows; ++i) // create test
                testData[i] = copy[i + numTrainRows];
        } // MakeTrainTest

        static void ShowVector(double[] vector, int decimals, int valsPerRow, bool newLine)
        {
            for (int i = 0; i < vector.Length; ++i)
            {
                if (i % valsPerRow == 0) Console.WriteLine("");
                Console.Write(vector[i].ToString("F" + decimals).PadLeft(decimals + 4) + " ");
            }
            if (newLine == true) Console.WriteLine("");
        }

        static void ShowMatrix(int[][] matrix, int numRows, bool indices)
        {
            for (int i = 0; i < numRows; ++i)
            {
                if (indices == true)
                    Console.Write("[" + i.ToString().PadLeft(2) + "]   ");
                for (int j = 0; j < matrix[i].Length; ++j)
                {
                    Console.Write(matrix[i][j] + " ");
                }
                Console.WriteLine("");
            }
            int lastIndex = matrix.Length - 1;
            if (indices == true)
                Console.Write("[" + lastIndex.ToString().PadLeft(2) + "]   ");
            for (int j = 0; j < matrix[lastIndex].Length; ++j)
                Console.Write(matrix[lastIndex][j] + " ");
            Console.WriteLine("\n");
        }

        static void ShuffleObservations(int[][] trainData) // Fisher-Yates shuffle algorithm
        {
            for (int i = 0; i < trainData.Length; ++i)
            {
                int r = rnd.Next(i, trainData.Length);
                int[] tmp = trainData[r];
                trainData[r] = trainData[i];
                trainData[i] = tmp;
            }
        }
    }

    public class Winnow
    {
        private int numInput;
        private double[] weights;
        private double threshold; // to determine Y = 0 or 1
        private double alpha; // increase/decrase factor
        private static Random rnd;

        public Winnow(int numInput, int rndSeed)
        {
            this.numInput = numInput;
            this.weights = new double[numInput];
            for (int i = 0; i < weights.Length; ++i)
                weights[i] = numInput / 2.0;
            this.threshold = 1.0 * numInput;
            this.alpha = 1.1;
            rnd = new Random(rndSeed);
        }

        public int ComputeY(int[] xValues)
        {
            double sum = 0.0;
            for (int i = 0; i < numInput; ++i)
                sum += weights[i] * xValues[i];
            if (sum > this.threshold)
                return 1;
            else
                return 0;
        }

        public double[] TrainWeights(int[][] trainData)
        {
            int[] xValues = new int[numInput];
            int target;
            int computed;
            ShuffleObservations(trainData);
            for (int i = 0; i < trainData.Length; ++i)
            {
                Array.Copy(trainData[i], xValues, numInput); // get the inputs
                target = trainData[i][numInput]; // last value is target
                computed = ComputeY(xValues);

                if (computed == 1 && target == 0) // need to decrease weights
                {
                    for (int j = 0; j < numInput; ++j)
                    {
                        if (xValues[j] == 0) continue; // no change when xi = 0
                        weights[j] = weights[j] / alpha; // demotion
                    }
                }
                else if (computed == 0 && target == 1) // need to increase weights
                {
                    for (int j = 0; j < numInput; ++j)
                    {
                        if (xValues[j] == 0) continue; // no change when xi = 0
                        weights[j] = weights[j] * alpha; // promotion
                    }
                }
            } // each training item

            double[] result = new double[numInput]; // = number weights
            Array.Copy(this.weights, result, numInput);
            return result;
        } // Train

        private static void ShuffleObservations(int[][] trainData) // Fisher-Yates shuffle algorithm
        {
            for (int i = 0; i < trainData.Length; ++i)
            {
                int r = rnd.Next(i, trainData.Length);
                int[] tmp = trainData[r];
                trainData[r] = trainData[i];
                trainData[i] = tmp;
            }
        }

        public double Accuracy(int[][] trainData)
        {
            int numCorrect = 0;
            int numWrong = 0;

            int[] xValues = new int[numInput];
            int target;
            int computed;

            for (int i = 0; i < trainData.Length; ++i)
            {
                Array.Copy(trainData[i], xValues, numInput); // get the inputs
                target = trainData[i][numInput]; // last value is target
                computed = ComputeY(xValues);
                // Console.WriteLine(computed);
                if (computed == target)
                    ++numCorrect;
                else
                    ++numWrong;
            }
            return (numCorrect * 1.0) / (numCorrect + numWrong);
        }
    } // Winnow
}
